from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.actionbar import ActionBar, ActionView, ActionButton, ActionPrevious
from kivy.uix.label import Label
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput
from kivy.uix.screenmanager import ScreenManager, Screen
import re
from os.path import isdir, join
from os import mkdir, listdir


class Item:
    def __init__(self, name, price, metric='kg'):
        assert name and price and metric
        self.name = name
        self.price = price
        self.metric = metric
    def __hash__(self):
        return self.name.__hash__()


class Items:
    def __init__(self, dct={}):
        self.__store = {}
        if isdir("/sdcard/"):
            Items.FILEDIR = "/sdcard/"
        else:
            Items.FILEDIR = "./"
        Items.APPDIR = join(Items.FILEDIR, "shophlpr")
        if not isdir(Items.APPDIR):
            mkdir(Items.APPDIR)
        for name, price in dct.items():
            assert name and price and not name.startswith(".")
            self.set(name, price, 'kg')
        self.update_from_file()

    def get_price(self, name):
        return self.__store[name.lower()].price

    def get_item(self, name):
        return self.__store[name.lower()]

    def exists(self, name):
        return bool(self.__store.get(name.lower()))

    def update_from_file(self):
        for name in listdir(Items.APPDIR):
            if name.startswith("."):
                continue
            text = open(join(Items.APPDIR, name), 'r').read()
            regex = re.compile('[a-z]+')
            price = float(regex.split(text)[0])
            metric = regex.search(text).group()
            self.__store[name] = Item(name, price, metric)

    def set(self, name, price, metric):
        name = name.lower()
        metric = metric.lower()
        self.__store[name] = Item(name, price, metric)
        filename = join(Items.APPDIR, name)
        with open(filename, 'w') as f:
            f.write(f'{price}{metric}')

    def __iter__(self):
        return self.__store.values().__iter__()


class FloatInput(TextInput):
    pat = re.compile('[^0-9]')
    def insert_text(self, substring, from_undo=False):
        pat = self.pat
        if '.' in self.text:
            s = re.sub(pat, '', substring)
        else:
            s = '.'.join([re.sub(pat, '', s) for s in substring.split('.', 1)])
        return super(FloatInput, self).insert_text(s, from_undo=from_undo)


class PriceInput(FloatInput):
    def __init__(self, **kwargs):
        self.item = kwargs.pop('item')
        super().__init__(**kwargs)
        self.bind(text=self.update_records)

    def update_records(self, _, __):
        if not self.text or not float(self.text):
            return
        items.set(self.item.name, float(self.text), self.item.metric)


class NewItem(Popup):
    def __init__(self, **kwargs):
        ok_action = kwargs.pop("ok_action")
        cancel_action = kwargs.pop("cancel_action", self.dismiss)
        super().__init__(**kwargs)
        layout = BoxLayout(orientation='vertical')
        action_layout = BoxLayout(orientation='horizontal')
        self.__item_name = TextInput()
        self.__item_price = FloatInput()
        layout.add_widget(self.__item_name)
        layout.add_widget(self.__item_price)
        ok_btn = Button(text="Ok")
        cancel_btn = Button(text="Cancel")
        ok_btn.bind(on_press=ok_action)
        cancel_btn.bind(on_press=cancel_action)
        action_layout.add_widget(cancel_btn)
        action_layout.add_widget(ok_btn)
        layout.add_widget(action_layout)
        self.add_widget(layout)

    def item_name(self):
        return self.__item_name.text.strip()

    def item_price(self):
        try:
            return float(self.__item_price.text)
        except ValueError:
            return 0


class CalculatePopup(Popup):
    def __init__(self, **kwargs):
        self.item = kwargs.pop("item")
        super().__init__(**kwargs)

        self.__item_price = Button(text=f"Rs. {self.item.price} / {self.item.metric}")
        self.__item_price.bind(on_press=self.edit_price)

        self.__price = FloatInput(text='0')
        self.__price.bind(text=self.update_weight)
        self.__price_btn = Button(text='Rs. 0')
        self.__price_btn.bind(on_press=self.price_selected)

        self.__weight = FloatInput(text='0')
        self.__weight.bind(text=self.update_price)
        self.__weight_btn = Button(text='0 g')
        self.__weight_btn.bind(on_press=self.weight_selected)

        self.layout = BoxLayout(orientation='vertical')
        self.layout.add_widget(self.__item_price)
        self.layout.add_widget(self.__price)
        self.layout.add_widget(self.__weight_btn)

        self.add_widget(self.layout)

        self.update_weight(None, None)

    def edit_price(self, _):
        popup = Popup(title=f"Edit {self.title} price per unit",
                      size_hint=(None, None), size=(600, 300)
                      )
        price_text = PriceInput(text=self.__item_price.text[4:-7], item=self.item)
        price_text.bind(text=self.update_item_price)
        price_text.bind(text=self.update_price)
        popup.add_widget(price_text)
        popup.open()

    def update_item_price(self, obj, text):
        self.__item_price.text = f"Rs. {text} per unit"

    def price_selected(self, _):
        self.__price.text = self.__price_btn.text[4:]
        self.layout.remove_widget(self.__price_btn)
        self.layout.remove_widget(self.__weight)
        self.layout.add_widget(self.__price)
        self.layout.add_widget(self.__weight_btn)

    def weight_selected(self, _):
        self.__weight.text = self.__weight_btn.text[:-2]
        self.layout.remove_widget(self.__price)
        self.layout.remove_widget(self.__weight_btn)
        self.layout.add_widget(self.__price_btn)
        self.layout.add_widget(self.__weight)

    def update_weight(self, __, _):
        try:
            self.__weight_btn.text = str(round(1000 / float(self.__item_price.text[4:-7]) * float(self.__price.text))) + ' g'
        except ValueError:
            self.__weight_btn.text = '0 g'
            return False
        return True

    def update_price(self, _, __):
        try:
            self.__price_btn.text = 'Rs. ' + str(round(float(self.__item_price.text[4:-7]) / 1000 * float(self.__weight.text)))
        except ValueError:
            self.__price_btn.text = 'Rs. 0'
            return False
        return True


class ShopHelperApp(App):
    def build(self):
        self.title = "Shop Helper"
        self.main_layout = BoxLayout(orientation='vertical')
        self.layout = GridLayout(cols=2, spacing=(15, 13), size_hint_y=None)
        # Make sure the height is such that there is something to scroll.
        self.main_layout.bind(minimum_height=self.layout.setter('height'))
        self.newitem_popup = NewItem(
            title="New Item", ok_action=self.set_and_dismiss,
            size_hint=(None, None), size=(400, 400)
        )
        actionbar = ActionBar(pos_hint={'bottom': 1})
        scroll_view = ScrollView(size_hint=(1, None), size=(Window.width, Window.height - actionbar.height))
        scroll_view.add_widget(self.layout)
        actionview = ActionView(use_separator=True)
        self.add_btn = ActionButton(text='+', font_size=50, important=True)
        self.add_btn.bind(on_press=self.newitem_popup.open)
        actionprev = ActionPrevious(title="My app")
        actionview.add_widget(self.add_btn)
        actionview.add_widget(actionprev)
        actionbar.add_widget(actionview)
        self.main_layout.add_widget(scroll_view)
        self.main_layout.add_widget(actionbar)
        self.item_btn = {}
        for item in items:
            self.create_new_item(item.name)
        return self.main_layout

    def calculate(self, btn):
        calculate_popup = CalculatePopup(
            title=btn.text,
            item=items.get_item(btn.text),
            size_hint=(None, None), size=(650, 450)
        )
        calculate_popup.open()
        return True

    def create_new_item(self, item_name):
        item_btn = Button(text=item_name.capitalize(), font_size=50,
                          size=(1, 100), size_hint=(1, None))
        item_btn.bind(on_press=self.calculate)
        self.item_btn[item_name.lower()] = item_btn
        self.layout.add_widget(item_btn)
        return True

    def set_and_dismiss(self, _):
        item_name = self.newitem_popup.item_name()
        item_price = self.newitem_popup.item_price()
        if (not item_name or item_name.startswith(".") or
            not item_price or items.exists(item_name)):
            return True
        items.set(item_name, item_price, 'kg')
        self.create_new_item(item_name)
        self.newitem_popup.dismiss()
        return True

if __name__ == '__main__':
    app = ShopHelperApp()
    items = Items()
    app.run()
